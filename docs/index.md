# ELIXIR Beacon v2 Network

Welcome to the **ELIXIR Beacon v2 Network documentation**!

After the approval of Beacon v2 as an official [GA4GH](https://www.ga4gh.org) standard and the success of the [ELIXIR Beacon v1 Network](https://beacon-network.elixir-europe.org/), there was a need of create a new implementation of the Beacon Network adding the functionalities of the new version.

With this new version, the Beacon Network is able to query genomic and pheno-clinical data of the patients across the many instances of the network.

In this documentation, all the functionalities and the deployment of the ELIXIR Beacon Network (EBN) will be explained.

### Collaborators:

<img src="images/biysc_bsc_logo.jpg.png" height="100" width="150">
<img src="images/biysc_crg_logo.jpg.png" height="50" width="100">
<img src="images/csc.png" height="50" width="100">
<img src="images/PN013300-logo.inb_.rgb_.hor_.en_.cutted_0_488.png" height="50" width="100">
<img src="images/SIB_logo.jpg" height="50" width="100">
<img src="images/University_of_Zurich_seal.svg.png" height="50" width="50">
<img src="images/ELIXIR_logo_white_background.png" height="50" width="100">
<img src="images/europe.png" height="50" width="100">
